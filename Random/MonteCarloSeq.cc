#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>

#include "CLHEP/Random/MTwistEngine.h"

const int totalDrawNumber = 2000000000;


int main (int argc, char ** argv)
{
  CLHEP::MTwistEngine * MT = new CLHEP::MTwistEngine();
  double piCount = 0;
  double piValues[10] = {};
  double x,y;
  double pi = 0;
  
  

  for(int j = 0; j < 10 ; j++)
  {
    MT->restoreStatus((std::string("MT_") + std::to_string(j)).c_str());
    piCount = 0;

    std::cout << "Simulation n° " << j+1 << std::endl;

    for(int i = 0 ; i < totalDrawNumber ; i++ )
    {
      x = MT->flat();
      y = MT->flat();

      if( (x*x + y*y) < 1 )
      {
        piCount += 1;
      }
    }
    piValues[j] = 4 * piCount / (double) totalDrawNumber;

    std::cout << "valeur : " << piValues[j] << std::endl;
  }

  pi = 0;
  for(int i = 0 ; i < 10; i++)
  {
    pi += piValues[i];
  }

  pi = pi/10;

  std::cout << "Simulation terminée, valeur de pi : " << pi << std::endl;

  delete MT;

  return 0;

}



