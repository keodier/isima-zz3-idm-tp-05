#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <exception>

#include "CLHEP/Random/MTwistEngine.h"

const int totalDrawNumber = 2000000000;


int main (int argc, char ** argv)
{
  CLHEP::MTwistEngine * MT = new CLHEP::MTwistEngine();

  double piCount = 0;
  double x,y;
  double piValue;
  

  if(argc == 2)
  {
    MT->restoreStatus(argv[1]);
  }

  for(int i = 0 ; i < totalDrawNumber ; i++ )
  {
    x = MT->flat();
    y = MT->flat();

    if( (x*x + y*y) < 1 )
    {
      piCount += 1;
    }
  }

  piValue = 4 * piCount / (double) totalDrawNumber;

  std::cout << piValue << std::endl;

  delete MT;

  return 0;

}



