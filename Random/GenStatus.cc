#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include <cstring>

#include "CLHEP/Random/MTwistEngine.h"

int NbFiles = 10;
int NbTiragesUneFile = 2000000000;
std::string BaseName = "MT_";

int main (int argc, char ** argv)
{
  if(argc == 4)
  {
    NbFiles = atoi(argv[1]);
    NbTiragesUneFile = atoi(argv[2]);
    BaseName = std::string(argv[3]);
  }
    // Initialisation par defaut de Mersenne Twister
  CLHEP::MTwistEngine * MT = new CLHEP::MTwistEngine();

  std::cout << "Generation de " << NbFiles << " fichiers avec " << NbTiragesUneFile << " tirages entre chaque : " << std::endl;

  for(int j = 0 ; j < NbFiles ; j++)
  {

    for(int i = 0; i < NbTiragesUneFile; i++)
    { 
      MT->flat();
    }

    std::cout << "generation fichier n° " << j + 1 << " Terminée"  << std::endl;

    MT->saveStatus((BaseName + std::to_string(j)).c_str());
  }
  
  delete MT;

  return 0;
}


