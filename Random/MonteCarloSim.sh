#!/bin/bash$
#g++ -o MonteCarloSim MonteCarloSim.cc -I./include -L./lib -lCLHEP-Random-2.1.0.0 -static
#g++ -o PiCompile PiCompile.cc
compteur=0
for ((i= 0 ; i < 10; i++ ));
do
    echo "Simulation $i lancée"
    ./MonteCarloSim MT_$i > Pi_$i &
done
wait
echo "Calcul de pi :"
./PiCompile
