#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <exception>

#include "CLHEP/Random/MTwistEngine.h"

int nbFiles;
int nbDrawOneFile;
std::string baseName;

int main (int argc, char ** argv)
{
  if(argc == 4)
  {
    nbFiles = atoi(argv[1]);
    nbDrawOneFile = atoi(argv[2]);
    baseName = std::string(argv[3]);

    CLHEP::MTwistEngine * MT_Exec = new CLHEP::MTwistEngine();
    CLHEP::MTwistEngine * MT_Test = new CLHEP::MTwistEngine( *MT_Exec);

    try{
      for(int j = -1 ; j < nbFiles ; j++)
      {
        if(j > -1)
        {
          std::cout << "test fichier " << baseName << j << std::endl;
          MT_Test->restoreStatus((baseName + std::to_string(j)).c_str());
        }

        for(int i = 0; i < nbDrawOneFile; i++)
        { 
          if(MT_Exec->flat() != MT_Test->flat())
          {
            throw std::exception();
          }
        }        
      }
    }
    catch(std::exception ex)
    {
      std::cout << "Erreur verification" << std::endl;
    }
    
    
  
    delete MT_Exec;
    delete MT_Test;

    return 0;

  }
  else
  {
    std::cout << "Nombre arguments incorrect" << std::endl;
    return -1;
  }




  
}


