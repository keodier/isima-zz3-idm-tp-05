#include <iostream>
#include <fstream>

int main (int argc, char ** argv)
{
  double piTemp;
  double pi;

  std::fstream fichier;

  for(int i = 0 ; i < 10 ; i++)
  {
    fichier.open("Pi_" + std::to_string(i), std::ios::in);
    fichier >> piTemp;
    pi += piTemp;
    fichier.close();
  }
  pi = pi / 10;
  
  std::cout << pi << std::endl;

  return 0;

}



