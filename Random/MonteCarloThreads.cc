#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include <thread>
#include <future>
#include <vector>

#include "CLHEP/Random/MTwistEngine.h"

const int totalDrawNumber = 2000000000;

// Fonction servant à simuler un calcul de pi en fonction du nom
double simulation (std::string nom)
  {
    CLHEP::MTwistEngine * MT = new CLHEP::MTwistEngine();
    double piCount = 0;
    double x,y;
    double piValue;
    
    MT->restoreStatus( nom.c_str());
    
    for(int i = 0 ; i < totalDrawNumber ; i++ )
    {
      x = MT->flat();
      y = MT->flat();

      if( (x*x + y*y) < 1 )
      {
        piCount += 1;
      }
    }

    piValue = 4 * piCount / (double) totalDrawNumber;
    delete MT;
    return piValue;
  };




int main (int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  // On stocke les simulations et les resultats dans des vector
  std::vector<std::thread> piThreads;
  std::vector<std::future<double>> piResults;

  // Valeur de retour de pi
  double piValue = 0;
  double piValueTemp[10];

  // Lancement des differentes simulations
  for(int i = 0; i < 10 ; i++ )
  {


    // Declaration d'une lambda propre à l'essai qui utilise 
    // le nom en variable (capture de l'indice i)
    auto simLambda = [i] (std::promise<double> && p ) 
    { 
      std::cout << "[" << i << "] running" << std::endl;

      p.set_value(simulation("MT_"+ std::to_string(i)));

      std::cout << "[" << i << "] finished" << std::endl;
    };

    // On cree une promise vide qui servira au lancement du thread
    std::promise<double> promise;

    
    piResults.push_back(promise.get_future());
    piThreads.push_back(std::thread(simLambda, std::move(promise)));

  }
  
  // On accumule les resultats de tous les threads dans un tableau statique
  for(int i = 0 ; i < 10 ; i++ )
  {
    piValueTemp[i] = piResults[i].get();
  }


  // on attend la fin des processus
  for(int i = 0 ; i < 10 ; i++ )
  {
    piThreads[i].join();
  }

  // on calcule pi
  for(int i = 0 ; i < 10 ; i++ )
  {
    piValue += piValueTemp[i];
  }


  std::cout << "valeur finale : " << piValue/10 << std::endl;


  return 1;
}










